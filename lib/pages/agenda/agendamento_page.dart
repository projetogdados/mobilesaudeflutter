import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';

class AgendamentoPage extends StatefulWidget {
  const AgendamentoPage({Key? key}) : super(key: key);

  @override
  _AgendamentoPageState createState() => _AgendamentoPageState();
}

class _AgendamentoPageState extends State<AgendamentoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Agendamento"),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Container(
                  child: Text(
                    "Dr. José Souza da Silva",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                Container(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 16.0),
                    child: CalendarCarousel(
                      weekendTextStyle: TextStyle(
                        color: Colors.red,
                      ),
                      thisMonthDayBorderColor: Colors.grey,
                      customDayBuilder: (
                        bool isSelectable,
                        int index,
                        bool isSelectedDay,
                        bool isToday,
                        bool isPrevMonthDay,
                        TextStyle textStyle,
                        bool isNextMonthDay,
                        bool isThisMonthDay,
                        DateTime day,
                      ) {
                        if (day.day == 15) {
                          return Center(
                            child: Icon(Icons.local_airport),
                          );
                        } else {
                          return null;
                        }
                      },
                      weekFormat: false,
                      height: 420.0,
                      daysHaveCircularBorder: false,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
