import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ubersaude/pages/agenda/atendimento_page.dart';

class AjudaPage extends StatelessWidget {
  const AjudaPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ajuda"),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            const SizedBox(height: 20),
            Container(
              child: Text(
                "Olá Johnny, como podemos te ajudar?",
                style: TextStyle(fontSize: 18),
              ),
            ),
            const SizedBox(height: 20),
            Container(
              width: double.infinity,
              child: ElevatedButton(
                child: const Text("MÉDICO"),
                style: ElevatedButton.styleFrom(
                  primary: Colors.orange.shade500,
                  padding: const EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 15,
                    bottom: 15,
                  ),
                ),
                onPressed: () {
                  Get.to(() => AtendimentoPage());
                },
              ),
            ),
            const SizedBox(height: 20),
            Container(
              width: double.infinity,
              child: ElevatedButton(
                child: const Text("EXAMES"),
                style: ElevatedButton.styleFrom(
                  primary: Colors.orange.shade500,
                  padding: const EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 15,
                    bottom: 15,
                  ),
                ),
                onPressed: () {
                  Get.to(() => AtendimentoPage());
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
