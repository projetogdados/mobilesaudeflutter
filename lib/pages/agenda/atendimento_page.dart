import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ubersaude/pages/agenda/agendamento_page.dart';

class AtendimentoPage extends StatelessWidget {
  const AtendimentoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Atendimento"),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            const SizedBox(height: 20),
            Container(
              child: Text(
                "Quando você desejaria de ser atendido?",
                style: TextStyle(fontSize: 18),
              ),
            ),
            const SizedBox(height: 20),
            Container(
              width: double.infinity,
              child: ElevatedButton(
                child: const Text("AGORA MESMO"),
                style: ElevatedButton.styleFrom(
                  primary: Colors.orange.shade500,
                  padding: const EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 15,
                    bottom: 15,
                  ),
                ),
                onPressed: () {
                  Get.to(() => AgendamentoPage());
                },
              ),
            ),
            const SizedBox(height: 20),
            Container(
              width: double.infinity,
              child: ElevatedButton(
                child: const Text("PREFIRO AGENDAR"),
                style: ElevatedButton.styleFrom(
                  primary: Colors.orange.shade500,
                  padding: const EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 15,
                    bottom: 15,
                  ),
                ),
                onPressed: () {
                  Get.to(() => AgendamentoPage());
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
