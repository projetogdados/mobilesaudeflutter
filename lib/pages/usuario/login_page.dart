import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ubersaude/pages/agenda/ajuda_page.dart';
import 'package:ubersaude/pages/paciente/paciente_form_page.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          child: Form(
            child: Column(
              children: [
                const SizedBox(height: 20),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: const Icon(Icons.account_circle_outlined, size: 50),
                ),
                const SizedBox(height: 0),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: const Text(
                    "Conectando você com a saúde",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    decoration: const InputDecoration(
                        hintText: "entre com o cpf",
                        suffixIcon: Icon(Icons.email_outlined)),
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    decoration: const InputDecoration(
                        hintText: "entre com a senha",
                        suffixIcon: Icon(Icons.visibility_outlined)),
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  padding: const EdgeInsets.all(10),
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                      padding: const EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 15,
                        bottom: 15,
                      ),
                    ),
                    child: const Text("Entrar"),
                    onPressed: () {
                      Get.to(() => AjudaPage());
                    },
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  width: double.infinity,
                  child: ElevatedButton(
                    child: const Text("CADASTRAR"),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.orange.shade500,
                      padding: const EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 15,
                        bottom: 15,
                      ),
                    ),
                    onPressed: () {
                      Get.to(() => PacienteFormPage());
                    },
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
