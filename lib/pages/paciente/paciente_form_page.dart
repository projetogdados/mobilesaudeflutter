import 'package:flutter/material.dart';

class PacienteFormPage extends StatefulWidget {
  const PacienteFormPage({Key? key}) : super(key: key);

  @override
  _PacienteFormPageState createState() => _PacienteFormPageState();
}

class _PacienteFormPageState extends State<PacienteFormPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cadastro de paciente"),
      ),
      body: buildListView(),
    );
  }

  ListView buildListView() {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          child: Form(
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(10),
                  child: const Text(
                    "Conectando você com a saúde",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    decoration: const InputDecoration(
                        hintText: "Entre com o nome completo",
                        suffixIcon: Icon(Icons.account_circle_outlined)),
                  ),
                ),
                const SizedBox(height: 15),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    decoration: const InputDecoration(
                        hintText: "Entre com o rg",
                        suffixIcon: Icon(Icons.email_outlined)),
                  ),
                ),
                const SizedBox(height: 15),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    decoration: const InputDecoration(
                        hintText: "entre com o cpf",
                        suffixIcon: Icon(Icons.vpn_key_outlined)),
                  ),
                ),
                const SizedBox(height: 15),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    decoration: const InputDecoration(
                        hintText: "entre com o email",
                        suffixIcon: Icon(Icons.email_outlined)),
                  ),
                ),
                const SizedBox(height: 15),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    decoration: const InputDecoration(
                        hintText: "entre com a senha",
                        suffixIcon: Icon(Icons.visibility_outlined)),
                  ),
                ),
                const SizedBox(height: 15),
                Container(
                  padding: const EdgeInsets.all(10),
                  width: double.infinity,
                  child: ElevatedButton(
                    child: const Text("CADASTRAR"),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.orange.shade500,
                      padding: const EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 15,
                        bottom: 15,
                      ),
                    ),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
