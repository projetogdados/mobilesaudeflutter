import 'package:get/get.dart';
import 'package:ubersaude/core/repository/prestador_repository.dart';

class PrestadorController extends GetxController {
  PrestadorRepository? prestadorRepository;

  PrestadorController({this.prestadorRepository}) {
    prestadorRepository = PrestadorRepository();
  }
}
