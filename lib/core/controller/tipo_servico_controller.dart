import 'package:get/get.dart';
import 'package:ubersaude/core/repository/tipo_servico_repository.dart';

class TipoServicoController extends GetxController {
  TipoServicoRepository? tipoServicoRepository;

  TipoServicoController({this.tipoServicoRepository}) {
    tipoServicoRepository = TipoServicoRepository();
  }
}
