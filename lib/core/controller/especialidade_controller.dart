import 'package:get/get.dart';
import 'package:ubersaude/core/repository/especialidade_repository.dart';

class EspecialidadeController extends GetxController {
  EspecialidadeRepository? especialidadeRepository;

  EspecialidadeController({this.especialidadeRepository}) {
    especialidadeRepository = EspecialidadeRepository();
  }


}
