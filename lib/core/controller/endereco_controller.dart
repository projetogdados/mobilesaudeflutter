import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:ubersaude/core/model/endereco.dart';
import 'package:ubersaude/core/repository/endereco_repository.dart';

class EnderecoController extends GetxController {
  EnderecoRepository? enderecoRepository;

  EnderecoController({this.enderecoRepository}) {
    enderecoRepository = EnderecoRepository();
  }

  var enderecos = <Endereco>[].obs;

  int? endereco;

  Future<List<Endereco>?> getAll() async {
    return enderecos.value = (await (enderecoRepository!.getAll()))!;
  }

  Future<int?> create(Endereco p) async {
    try {
      endereco = await enderecoRepository!.create(p.toJson());
      if (endereco == null) {
        printError(info: "${endereco}");
      } else {
        return endereco;
      }
    } on DioError catch (e) {
      printError(info: "${e}");
    }
  }

  Future<int?> edit(int id, Endereco p) async {
    try {
      endereco = await enderecoRepository!.update(id, p.toJson());
      return endereco;
    } on DioError catch (e) {
      printError(info: "${e}");
    }
  }
}
