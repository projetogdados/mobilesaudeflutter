import 'package:get/get.dart';
import 'package:ubersaude/core/repository/instituicao_repository.dart';

class InstituicaoController extends GetxController {
  InstituicaoRepository? instituicaoRepository;

  InstituicaoController({this.instituicaoRepository}) {
    instituicaoRepository = InstituicaoRepository();
  }
}
