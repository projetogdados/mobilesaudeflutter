import 'package:get/get.dart';
import 'package:ubersaude/core/repository/paciente_repository.dart';

class PacienteController extends GetxController {
  PacienteRepository? pacienteRepository;

  PacienteController({this.pacienteRepository}) {
    pacienteRepository = PacienteRepository();
  }
}
