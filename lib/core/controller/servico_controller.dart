import 'package:get/get.dart';
import 'package:ubersaude/core/repository/servico_repository.dart';

class ServicoController extends GetxController {
  ServicoRepository? servicoRepository;

  ServicoController({this.servicoRepository}) {
    servicoRepository = ServicoRepository();
  }
}
