import 'package:get/get.dart';
import 'package:ubersaude/core/repository/prontuario_repository.dart';

class ProntuarioController extends GetxController {
  ProntuarioRepository? prontuarioRepository;

  ProntuarioController({this.prontuarioRepository}) {
    prontuarioRepository = ProntuarioRepository();
  }
}
