import 'package:ubersaude/core/model/pessoa.dart';

class Instituicao extends Pessoa {
  String? cnpj;
  String? razaoSocial;
  String? url;

  Instituicao({
    this.cnpj,
    this.razaoSocial,
    this.url,
  });
}
