import 'package:ubersaude/core/model/especialidade.dart';
import 'package:ubersaude/core/model/paciente.dart';
import 'package:ubersaude/core/model/prestador.dart';
import 'package:ubersaude/core/model/status_servico.dart';
import 'package:ubersaude/core/model/tipo_servico.dart';

class Servico {
  int? id;
  String? descricao;
  DateTime? dataHora;
  DateTime? dataHoraInicio;
  DateTime? dataHoraFinal;
  Paciente? paciente;
  Prestador? prestador;
  TipoServico? tipoServico;
  Especialidade? especialidade;
  StatusServico? statusServico;

  Servico({
    this.id,
    this.descricao,
    this.dataHora,
    this.dataHoraInicio,
    this.dataHoraFinal,
    this.paciente,
    this.prestador,
    this.tipoServico,
    this.especialidade,
    this.statusServico,
  });
}
