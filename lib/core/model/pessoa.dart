import 'package:ubersaude/core/model/endereco.dart';

class Pessoa {
  int? id;
  String? nome;
  String? email;
  String? telefone;
  String? foto;
  Endereco? endereco;
  bool? status;

  Pessoa({
    this.id,
    this.nome,
    this.email,
    this.telefone,
    this.foto,
    this.endereco,
    this.status,
  });

  Pessoa.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nome = json['nome'];
    email = json['email'];
    telefone = json['telefone'];
    foto = json['foto'];

    endereco = json['endereco'] != null
        ? new Endereco.fromJson(json['endereco'])
        : null;

    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nome'] = this.nome;
    data['email'] = this.email;
    data['telefone'] = this.telefone;
    data['foto'] = this.foto;

    if (this.endereco != null) {
      data['endereco'] = this.endereco!.toJson();
    }

    data['status'] = this.status;

    return data;
  }
}
