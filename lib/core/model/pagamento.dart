import 'package:ubersaude/core/model/forma_pagamento.dart';
import 'package:ubersaude/core/model/status_pagamento.dart';

class Pagamento {
  int? id;
  double? valorUnitario;
  double? valorTotal;
  double? desconto;
  DateTime? data;
  DateTime? hora;
  FormaPagamento? formaPagamento;
  StatusPagamento? statusPagamento;

  Pagamento({
    this.id,
    this.valorUnitario,
    this.valorTotal,
    this.desconto,
    this.data,
    this.hora,
    this.formaPagamento,
    this.statusPagamento,
  });
}
