import 'package:ubersaude/core/model/pessoa.dart';

class Paciente extends Pessoa {
  int? id;
  String? cpf;
  String? rg;
  String? orgaoExpedidor;
  DateTime? dataNascimento;
  String? genero;

  Paciente({
    this.id,
    this.cpf,
    this.orgaoExpedidor,
    this.dataNascimento,
    this.genero,
  });

  Paciente.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    cpf = json['cpf'];
    rg = json['rg'];
    orgaoExpedidor = json['orgaoExpedidor'];
    dataNascimento = DateTime.tryParse(json['dataNascimento'].toString());
    genero = json['genero'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['cpf'] = this.cpf;
    data['rg'] = this.rg;
    data['orgaoExpedidor'] = this.orgaoExpedidor;
    data['dataNascimento'] = this.dataNascimento!.toIso8601String();
    data['genero'] = this.genero;
    return data;
  }
}
