import 'package:ubersaude/core/model/pessoa.dart';

class Prestador extends Pessoa{
  String? cpf;
  String? crm;

  Prestador({this.cpf, this.crm});

  Prestador.fromJson(Map<String, dynamic> json) {
    cpf = json['cpf'];
    crm = json['crm'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cpf'] = this.cpf;
    data['crm'] = this.crm;
    return data;
  }
}
