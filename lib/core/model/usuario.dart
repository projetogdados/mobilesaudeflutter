import 'package:ubersaude/core/model/pessoa.dart';

class Usuario {
  int? id;
  String? user;
  String? senha;
  String? confirmaSenha;
  Pessoa? pessoa;

  Usuario({this.id, this.user, this.senha, this.confirmaSenha, this.pessoa});

  Usuario.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    user = json['user'];
    senha = json['senha'];
    confirmaSenha = json['confirmaSenha'];
    pessoa =
        json['pessoa'] != null ? new Pessoa.fromJson(json['pessoa']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user'] = this.user;
    data['senha'] = this.senha;
    data['confirmaSenha'] = this.confirmaSenha;
    if (this.pessoa != null) {
      data['pessoa'] = this.pessoa!.toJson();
    }
    return data;
  }
}
