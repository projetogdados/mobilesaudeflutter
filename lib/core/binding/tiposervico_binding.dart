import 'package:get/get.dart';
import 'package:ubersaude/core/controller/tipo_servico_controller.dart';
import 'package:ubersaude/core/repository/tipo_servico_repository.dart';

class TipoServicoSaidaBinding extends Bindings {
  TipoServicoRepository? tipoServicoRepository;

  TipoServicoSaidaBinding() {
    tipoServicoRepository = Get.put(TipoServicoRepository());
  }

  @override
  void dependencies() {
    Get.lazyPut<TipoServicoController>(() =>
        TipoServicoController(tipoServicoRepository: tipoServicoRepository));
  }
}
