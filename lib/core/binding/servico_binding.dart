import 'package:get/get.dart';
import 'package:ubersaude/core/controller/servico_controller.dart';
import 'package:ubersaude/core/repository/servico_repository.dart';

class CartaoBinding extends Bindings {
  ServicoRepository? servicoRepository;

  CartaoBinding() {
    servicoRepository = Get.put(ServicoRepository());
  }

  @override
  void dependencies() {
    Get.lazyPut<ServicoController>(
        () => ServicoController(servicoRepository: servicoRepository));
  }
}
