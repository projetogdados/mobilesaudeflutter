import 'package:get/get.dart';
import 'package:ubersaude/core/controller/especialidade_controller.dart';
import 'package:ubersaude/core/repository/especialidade_repository.dart';

class EspecialidadeBinding extends Bindings {
  EspecialidadeRepository? especialidadeRepository;

  EspecialidadeBinding() {
    especialidadeRepository = Get.put(EspecialidadeRepository());
  }

  @override
  void dependencies() {
    Get.lazyPut<EspecialidadeController>(
        () => EspecialidadeController(especialidadeRepository: especialidadeRepository));
  }
}
