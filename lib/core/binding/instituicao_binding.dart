import 'package:get/get.dart';
import 'package:ubersaude/core/controller/instituicao_controller.dart';
import 'package:ubersaude/core/repository/instituicao_repository.dart';

class InstituicaoBinding extends Bindings {
  InstituicaoRepository? instituicaoRepository;

  InstituicaoBinding() {
    instituicaoRepository = Get.put(InstituicaoRepository());
  }

  @override
  void dependencies() {
    Get.lazyPut<InstituicaoController>(() =>
        InstituicaoController(instituicaoRepository: instituicaoRepository));
  }
}
