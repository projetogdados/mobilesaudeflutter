import 'package:get/get.dart';
import 'package:ubersaude/core/controller/prontuario_controller.dart';
import 'package:ubersaude/core/repository/prontuario_repository.dart';

class ProntuarioBinding extends Bindings {
  ProntuarioRepository? prontuarioRepository;

  ProntuarioBinding() {
    prontuarioRepository = Get.put(ProntuarioRepository());
  }

  @override
  void dependencies() {
    Get.lazyPut<ProntuarioController>(
      () => ProntuarioController(prontuarioRepository: prontuarioRepository),
    );
  }
}
