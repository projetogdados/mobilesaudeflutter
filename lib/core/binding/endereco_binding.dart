import 'package:get/get.dart';
import 'package:ubersaude/core/controller/endereco_controller.dart';
import 'package:ubersaude/core/repository/endereco_repository.dart';

class EnderecoBinding extends Bindings {
  EnderecoRepository? enderecoRepository;

  EnderecoBinding() {
    enderecoRepository = Get.put(EnderecoRepository());
  }

  @override
  void dependencies() {
    Get.lazyPut<EnderecoController>(() => EnderecoController(enderecoRepository: enderecoRepository));
  }
}
