import 'package:get/get.dart';
import 'package:ubersaude/core/controller/endereco_controller.dart';
import 'package:ubersaude/core/controller/especialidade_controller.dart';
import 'package:ubersaude/core/controller/instituicao_controller.dart';
import 'package:ubersaude/core/controller/paciente_controller.dart';
import 'package:ubersaude/core/controller/prestador_controller.dart';
import 'package:ubersaude/core/controller/prontuario_controller.dart';
import 'package:ubersaude/core/controller/servico_controller.dart';
import 'package:ubersaude/core/repository/endereco_repository.dart';
import 'package:ubersaude/core/repository/especialidade_repository.dart';
import 'package:ubersaude/core/repository/instituicao_repository.dart';
import 'package:ubersaude/core/repository/paciente_repository.dart';
import 'package:ubersaude/core/repository/prestador_repository.dart';
import 'package:ubersaude/core/repository/prontuario_repository.dart';
import 'package:ubersaude/core/repository/servico_repository.dart';
import 'package:ubersaude/core/repository/tipo_servico_repository.dart';

class HomeBinding extends Bindings {
  EnderecoRepository? enderecoRepository;
  EspecialidadeRepository? especialidadeRepository;
  InstituicaoRepository? instituicaoRepository;
  PacienteRepository? pacienteRepository;
  PrestadorRepository? prestadorRepository;
  ProntuarioRepository? prontuarioRepository;
  ServicoRepository? servicoRepository;
  TipoServicoRepository? tipoServicoRepository;

  HomeBinding() {
    enderecoRepository = Get.put(EnderecoRepository());
    especialidadeRepository = Get.put(EspecialidadeRepository());
    instituicaoRepository = Get.put(InstituicaoRepository());
    pacienteRepository = Get.put(PacienteRepository());
    prestadorRepository = Get.put(PrestadorRepository());
    prontuarioRepository = Get.put(ProntuarioRepository());
    servicoRepository = Get.put(ServicoRepository());
    tipoServicoRepository = Get.put(TipoServicoRepository());
  }

  @override
  void dependencies() {
    Get.lazyPut<EnderecoController>(
      () => EnderecoController(enderecoRepository: enderecoRepository),
    );

    Get.lazyPut<EspecialidadeController>(
      () => EspecialidadeController(
          especialidadeRepository: especialidadeRepository),
    );

    Get.lazyPut<InstituicaoController>(
      () => InstituicaoController(instituicaoRepository: instituicaoRepository),
    );

    Get.lazyPut<EnderecoController>(
      () => EnderecoController(enderecoRepository: enderecoRepository),
    );

    Get.lazyPut<PacienteController>(
      () => PacienteController(pacienteRepository: pacienteRepository),
    );

    Get.lazyPut<PrestadorController>(
      () => PrestadorController(prestadorRepository: prestadorRepository),
    );

    Get.lazyPut<ProntuarioController>(
      () => ProntuarioController(prontuarioRepository: prontuarioRepository),
    );

    Get.lazyPut<ServicoController>(
      () => ServicoController(servicoRepository: servicoRepository),
    );
  }
}
