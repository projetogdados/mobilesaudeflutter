import 'package:get/get.dart';
import 'package:ubersaude/core/controller/prestador_controller.dart';
import 'package:ubersaude/core/repository/prestador_repository.dart';

class ClienteBinding extends Bindings {
  PrestadorRepository? prestadorRepository;

  ClienteBinding() {
    prestadorRepository = Get.put(PrestadorRepository());
  }

  @override
  void dependencies() {
    Get.lazyPut<PrestadorController>(
        () => PrestadorController(prestadorRepository: prestadorRepository));
  }
}
