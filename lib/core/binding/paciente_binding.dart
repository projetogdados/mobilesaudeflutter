import 'package:get/get.dart';
import 'package:ubersaude/core/controller/paciente_controller.dart';
import 'package:ubersaude/core/repository/paciente_repository.dart';

class PacienteBinding extends Bindings {
  PacienteRepository? pacienteRepository;

  PacienteBinding() {
    pacienteRepository = Get.put(PacienteRepository());
  }

  @override
  void dependencies() {
    Get.lazyPut<PacienteController>(
        () => PacienteController(pacienteRepository: pacienteRepository));
  }
}
