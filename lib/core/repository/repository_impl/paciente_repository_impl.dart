import 'package:ubersaude/core/model/paciente.dart';

abstract class PacienteRepositoryImpl {
  Future<List<Paciente>?> getAll();

  Future<int?> create(Map<String, dynamic> data);

  Future<int?> update(int id, Map<String, dynamic> data);
}
