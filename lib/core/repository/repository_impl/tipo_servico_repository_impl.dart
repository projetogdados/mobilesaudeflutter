import 'package:ubersaude/core/model/tipo_servico.dart';

abstract class TipoServicoRepositoryImpl {
  Future<List<TipoServico>?> getAll();

  Future<int?> create(Map<String, dynamic> data);

  Future<int?> update(int id, Map<String, dynamic> data);
}
