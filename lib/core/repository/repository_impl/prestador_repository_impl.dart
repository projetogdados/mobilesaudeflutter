import 'package:ubersaude/core/model/prestador.dart';

abstract class PrestadorRepositoryImpl {
  Future<List<Prestador>?> getAll();

  Future<int?> create(Map<String, dynamic> data);

  Future<int?> update(int id, Map<String, dynamic> data);
}
