import 'package:ubersaude/core/model/endereco.dart';

abstract class EnderecoRepositoryImpl {
  Future<List<Endereco>?> getAll();

  Future<int?> create(Map<String, dynamic> data);

  Future<int?> update(int id, Map<String, dynamic> data);
}
