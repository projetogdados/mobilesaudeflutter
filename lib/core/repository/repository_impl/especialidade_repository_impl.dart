import 'package:ubersaude/core/model/especialidade.dart';

abstract class EspecialidadeRepositoryImpl {
  Future<List<Especialidade>?> getAll();

  Future<int?> create(Map<String, dynamic> data);

  Future<int?> update(int id, Map<String, dynamic> data);
}
