
import 'package:ubersaude/core/model/instituicao.dart';

abstract class InstituicaoRepositoryImpl {
  Future<List<Instituicao>?> getAll();

  Future<int?> create(Map<String, dynamic> data);

  Future<int?> update(int id, Map<String, dynamic> data);
}
