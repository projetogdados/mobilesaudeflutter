import 'package:ubersaude/core/model/servico.dart';

abstract class ServicoRepositoryImpl {
  Future<List<Servico>?> getAll();

  Future<int?> create(Map<String, dynamic> data);

  Future<int?> update(int id, Map<String, dynamic> data);
}
