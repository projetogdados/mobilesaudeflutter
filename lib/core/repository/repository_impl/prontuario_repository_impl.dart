import 'package:ubersaude/core/model/prontuario.dart';

abstract class ProntuarioRepositoryImpl {
  Future<List<Prontuario>?> getAll();

  Future<int?> create(Map<String, dynamic> data);

  Future<int?> update(int id, Map<String, dynamic> data);
}
