import 'package:dio/dio.dart';
import 'package:ubersaude/api/dio/custon_dio.dart';
import 'package:ubersaude/core/model/endereco.dart';
import 'package:ubersaude/core/repository/repository_impl/endereco_repository_impl.dart';

class EnderecoRepository implements EnderecoRepositoryImpl {
  CustonDio dio = CustonDio();

  @override
  Future<int?> create(Map<String, dynamic> data) async {
    var response = await dio.client.post("/enderecos/create", data: data);
    return response.statusCode;
  }

  @override
  Future<List<Endereco>?> getAll() async {
    try {
      print("carregando enderecos");
      var response = await dio.client.get("/enderecos");
      return (response.data as List).map((c) => Endereco.fromJson(c)).toList();
    } on DioError catch (e) {
      print(e.message);
    }
    return null;
  }

  @override
  Future<int?> update(int id, Map<String, dynamic> data) async {
    var response = await dio.client.put("/enderecos/update/$id", data: data);
    return response.statusCode;
  }
}
