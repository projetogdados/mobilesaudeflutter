import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ubersaude/pages/usuario/login_page.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // uber saude mobile - dev
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: "UBERSAUDE",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
        brightness: Brightness.light,
        primaryColor: Colors.indigo.shade600,
        accentColor: Colors.yellow.shade600,
        scaffoldBackgroundColor: Colors.grey.shade100,
        inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.indigo.shade600),
          ),
          labelStyle: TextStyle(color: Colors.indigo.shade600, fontSize: 14),
        ),
      ),
      home: const MyHomePage(title: "UBERSAUDE"),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(widget.title),
      ),
      body: Login(),
    );
  }
}
